## Iteration
- Iteration through collections is a fundamental operation in Java.
- The foreach statement is a concise way to iterate through collections.
- It is essentially shorthand for a while loop that uses iterators.

- The foreach statement is a concise way to iterate through collections.
```java
for (String s : collection)
    StdOut.println(s);
```

```java
Iterator<String> i = collection.iterator();
while (i.hasNext()) {
    String s = i.next();
    StdOut.println(s);
}
```

## Implementation of Iteration
- Collections must implement an iterator() method that returns an Iterator object.
- The Iterator interface ( An interface in programming defines a set of methods that a class must implement. It serves as a contract, specifying what methods a class must provide without detailing their implementation. Interfaces promote abstraction, standardization, decoupling, polymorphism, and facilitate API design. ) includes hasNext() (this method returns true if there are more elements) and next() (this method returns the next element) methods.
- Java provides interfaces like Iterable<Item> and Iterator<Item> to express iteration methods.

```java
public interface Iterable<Item> {
    Iterator<Item> iterator(); 
}
```

```java
public interface Iterator<Item> {
    boolean hasNext();
    Item next();
    void remove();
}
```

# Understanding Interfaces with a Real-Life Scenario

Consider the following situation:

You are in the middle of a large, empty room, when a zombie suddenly attacks you.

You have no weapon.

Luckily, a fellow living human is standing in the doorway of the room.

"Quick!" you shout at him. "Throw me something I can hit the zombie with!"

Now consider:
You didn't specify (nor do you care) exactly what your friend will choose to toss;
...But it doesn't matter, as long as:

- It's something that can be tossed (He can't toss you the sofa)
- It's something that you can grab hold of (Let's hope he didn't toss a shuriken)
- It's something you can use to bash the zombie's brains out (That rules out pillows and such)

It doesn't matter whether you get a baseball bat or a hammer -
as long as it implements your three conditions, you're good.

To sum it up:

When you write an interface, you're basically saying: "I need something that..."


## Necessity of Iterators
- They provide a way to traverse through collections in a standardized manner.

## Benefits of Encapsulation
- Encapsulation allows for modularity, code reusability, and data integrity.
- It hides the internal details of a class and exposes only the necessary functionalities.
- Encapsulation is crucial for managing collections effectively in Java.
