# Abstract Data Types (ADTs)

- ADTs are blueprints for organizing and manipulating collections of objects.
- They include bags, queues, and stacks, each with specific rules for adding, removing, or examining objects.

# APIs

- Each ADT has a set of operations: adding items, checking if the collection is empty, and getting its size.
- Specific operations differ for each ADT, such as removing items from stacks and queues.

# Generics

- Java generics allow using ADTs with any type of data, utilizing autoboxing for primitive types.
- Overcoming limitations by introducing generics to create a flexible ADTs for any data type.
- A specific Java mechanism known as generics enables this capability. The notation <Item> after the class name in each of our APIs defines the name Item as a type parameter, a symbolic placeholder for some concrete type to be used by the client. You can read Stack<Item> as "stack of items." 

```java
Stack<String> stack = new Stack<String>();
stack.push("Test");
...
String next = stack.pop();
```

# Iterable Collections

- Iterable collections allow iterating through items easily without knowing implementation details.
- Clients can process items using a foreach loop, making code clearer and more compact.

```java
for (Transaction t : collection)
   StdOut.println(t);
```
# ADT Examples

- **Bag**: For collecting items without a specific removal order. Used for processing items in any order. (order doesn't matter)

```java
public class Bag<Item> implements Iterable<Item> {
   Bag()          create an empty bag
   void add(Item) add an item
   boolean isEmpty() is the bag empty?
   int size()      number of items in the bag
}
```

- **Queue**: Follows FIFO (First-In-First-Out) policy, useful for maintaining order in processing tasks.

```java
public class Queue<Item> implements Iterable<Item> {
   Queue()          create an empty queue
   void enqueue(Item) add an item
   Item dequeue()   remove the least recently added item
   boolean isEmpty() is the queue empty?
   int size()       number of items in the queue
}
```

- **Stack**: Follows LIFO (Last-In-First-Out) policy, suitable for organizing tasks or navigating sequences like web pages.

```java
public class Stack<Item> implements Iterable<Item> {
   Stack()          create an empty stack
   void push(Item)  add an item
   Item pop()       remove the most recently added item
   boolean isEmpty() is the stack empty?
   int size()       number of items in the stack
}
```

# Arithmetic Expression Evaluation

- Introduction to arithmetic expression evaluation using stacks.
- Explanation of Dijkstra's Two-Stack Algorithm.

## Dijkstra's Two-Stack Algorithm steps:
1. Push operands onto the operand stack.
2. Push operators onto the operator stack.
3. Ignore left parentheses.
4. On encountering a right parenthesis, pop an operator, pop the requisite number of operands, and push the result onto the operand stack.

```java
public class Evaluate {
   public static void main(String[] args) {
      Stack<String> ops = new Stack<String>();
      Stack<Double> vals = new Stack<Double>();
      while (!StdIn.isEmpty()) {
         String s = StdIn.readString();
         if      (s.equals("("))               ;
         else if (s.equals("+"))    ops.push(s);
         else if (s.equals("-"))    ops.push(s);
         else if (s.equals("*"))    ops.push(s);
         else if (s.equals("/"))    ops.push(s);
         else if (s.equals("sqrt")) ops.push(s);
         else if (s.equals(")")) {
            String op = ops.pop();
            double v = vals.pop();
            if      (op.equals("+"))    v = vals.pop() + v;
            else if (op.equals("-"))    v = vals.pop() - v;
            else if (op.equals("*"))    v = vals.pop() * v;
            else if (op.equals("/"))    v = vals.pop() / v;
            else if (op.equals("sqrt")) v = Math.sqrt(v);
            vals.push(v);
         }
         else vals.push(Double.parseDouble(s));
      }
      StdOut.println(vals.pop());
   }
}
```
# Fixed-Capacity Stack of Strings

- Initial implementation using a fixed-capacity array of strings.
- Simple API supporting basic operations like push and pop.
- Drawbacks include limited to strings, requires capacity specification, and lacks iteration support.

# Array Resizing

- Dynamic resizing addresses the fixed capacity issue by resizing the array as needed.
- Resize method implements resizing logic to accommodate more items efficiently.

# Summary

- ADTs provide blueprints for organizing and manipulating collections of objects.
- APIs define operations for adding, removing, or examining items in bags, queues, and stacks.
- Generics allow using ADTs with any data type, providing flexibility and clarity in code.
- Iterable collections support iteration through items using foreach loop, making code clearer and more compact.
- Arithmetic expression evaluation uses stacks to process expressions efficiently.
- Dijkstra's Two-Stack Algorithm simplifies arithmetic expression evaluation using two stacks.
- Implementing a stack with generics and dynamic resizing improves flexibility and efficiency in handling collections.
