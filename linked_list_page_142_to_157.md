## Linked Lists
Linked lists are fundamental data structures used for representing collections of data. They are recursive data structures that consist of nodes, where each node contains an item and a reference to the next node in the list. This recursive nature allows linked lists to represent sequences of items efficiently.

## Node Record
- Nodes in linked lists are typically implemented as records, which contain the item and a reference to the next node.
- Nodes are defined as nested classes within the main class implementing the linked list.
- Nodes are private to the class and not directly accessible by clients, promoting data abstraction.

## Building a Linked List
- A linked list is built by creating nodes and linking them together.
- Each node represents an item in the sequence, and the next reference points to the following node.
- Linked lists can represent sequences of items more flexibly than arrays, especially for insertion and removal operations.

## Insertion and Removal Operations
- Insertion at the beginning of a linked list involves creating a new node and updating the reference to the first node.
- Removal from the beginning simply requires updating the reference to the first node.
- Insertion at the end requires maintaining a reference to the last node and updating its next reference.
- These operations have a time complexity independent of the length of the list.

## Challenges and Limitations
- Operations like removing a given node or inserting before a given node are not easily handled with simple linked lists.
- Removing the last node requires traversing the entire list to find the node pointing to the last node, resulting in linear time complexity.
- Doubly-linked lists, where each node has references to both the previous and next nodes, are used to address these limitations.


## Traversal
Traversal in linked lists involves iterating through each node to process its item. This is similar to iterating through items in an array. We start at the first node and move to the next until we reach the end.

## Stack Implementation
A stack implemented with a linked list maintains its elements in a last-in-first-out (LIFO) order. Items are added and removed from the beginning of the list. The implementation tracks the number of items for efficiency.

```java
public class Stack<Item> implements Iterable<Item> {
    private Node first; // top of stack
    private int N; // number of items

    private class Node {
        Item item;
        Node next;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return N;
    }

    public void push(Item item) {
        Node oldfirst = first;
        first = new Node();
        first.item = item;
        first.next = oldfirst;
        N++;
    }

    public Item pop() {
        Item item = first.item;
        first = first.next;
        N--;
        return item;
    }
}
```

## Queue Implementation
Similarly, a queue implemented with a linked list maintains its elements in a first-in-first-out (FIFO) order. Items are added to the end and removed from the beginning of the list.

```java
public class Queue<Item> implements Iterable<Item> {
    private Node first; // beginning of queue
    private Node last; // end of queue
    private int N; // number of items

    private class Node {
        Item item;
        Node next;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return N;
    }

    public void enqueue(Item item) {
        Node oldlast = last;
        last = new Node();
        last.item = item;
        last.next = null;
        if (isEmpty()) first = last;
        else oldlast.next = last;
        N++;
    }

    public Item dequeue() {
        Item item = first.item;
        first = first.next;
        if (isEmpty()) last = null;
        N--;
        return item;
    }
}
```

## Bag Implementation
A bag implemented with a linked list allows adding items without specifying their order. Items are added to the beginning of the list.

```java
public class Bag<Item> implements Iterable<Item> {
    private Node first; // beginning of bag
    private int N; // number of items

    private class Node {
        Item item;
        Node next;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return N;
    }

    public void add(Item item) {
        Node oldfirst = first;
        first = new Node();
        first.item = item;
        first.next = oldfirst;
        N++;
    }
}
```
## Data Structures
Arrays provide immediate access to items but require preallocation, while linked lists use space proportional to their size but offer dynamic allocation. Different data structures serve different purposes and have trade-offs between space and access time.
